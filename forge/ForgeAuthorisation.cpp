#include <ForgeAuthorisation.h>
#include <QDebug>
#include <forge.h>
#include <QDir>

ForgeUser initUser(const std::string& login)
{
    ;
}


bool containsUser(const std::string& login)
{

}


bool isCorrectPassword(const std::string& password, const std::string& login)
{
    const int MIN_PSWD_SIZE = 5;
    if ( password.size() < MIN_PSWD_SIZE ) {
        qDebug() << "Forge > Minimum size of password is 5 symbols.\n";
        return false;
    }

    if ( password == login ) {
        qDebug() << "Forge > Password and login can't be equal.\n";
        return false;
    }

    return true;
}


bool isCorrectLogin(const std::string& login)
{
//    if ( containsUser(login) ) {
//        qDebug() << "Forge > User with the same login is already exists.\n";
//        return false;
//    }

//    if (login == ' ') {
//        qDebug() << "Forge > Login is not correct.\n";
//        return false;
//    }

    return true;
}


bool createUser()
{
    std::string login;
    qDebug() << "Forge > Atention! Enter \"exit\" to stop create a new user.\n";
    qDebug() << "Forge. User create > Please enter login: \n";
    std::getline(std::cin, login);
    if (login == "exit")
        return false;
    while ( !isCorrectLogin(login) )
    {
        qDebug() << "Please enter another login: ";
        std::getline(std::cin, login);
        if (login == "exit")
            return false;
    }

    std::string password;
    qDebug() << "Forge. User create > Please enter password: ";
    std::getline(std::cin, password);
    if (password == "exit")
        return false;
    while ( !isCorrectPassword(password, login) )
    {
        qDebug() << "Please enter another password: ";
        std::getline(std::cin, password);
        if (password == "exit")
            return false;
    }


    ForgeUser forgeUser(login, password);
    QDir dir("ForgeDB_Store");
    if ( !dir.exists() )
        dir.mkdir("ForgeDB_Store");
    else
        dir.cd("ForgeDB_Store");

    BasicForge<QString, QString> usersDB("ForgeDB_UsersDB");
    ;
}

