#ifndef FORGE_CPP
#define FORGE_CPP

#include <forge.h>
#include <iostream>
#include <QDebug>
#include <QDataStream>
#include <QList>
#include <QFile>
#include <math.h>
#include <QStack>
#include <ForgeLanguage.h>
#include <QFileInfo>
#include <QDateTime>


//template<class keyType, class valueType>
//BasicForge<keyType, valueType>::BasicForge()
//{
//    isPutCalled = false;
//    userFileName = "userData";
//}


template<class keyType, class valueType>
BasicForge<keyType, valueType>::BasicForge(const QString& fileName)
{
    QFile userFile(fileName);
    if ( !userFile.exists() ) {
        if ( !userFile.open(QIODevice::ReadWrite) ) {
            m_userFileName = "userData";
            qDebug() << "Can't create file. All data will be recorded in userData";
            userFile.setFileName("userData");
            if ( !userFile.open(QIODevice::ReadWrite) )
                qDebug() << "Can't create userData. Your data will be don't saved";
        }
        else
            m_userFileName = fileName;
    }

    else {
        if ( !userFile.open(QIODevice::ReadWrite) ) {
            m_userFileName = "userData";
            qDebug() << "Can't open previously existing file. All data will be recorded in userData";
            userFile.setFileName("userData");
            if ( !userFile.open(QIODevice::ReadWrite) )
                qDebug() << "Can't create userData. Your data will not be saved";
        }
        else
            m_userFileName = fileName;
    }
}


template<class keyType, class valueType>
BasicForge<keyType, valueType>::~BasicForge()
{
    if ( !m_buffer.isEmpty() )
        saveCurrentData();

    QFileInfo info(m_userFileName + "_keys");
    if ( info.lastModified() != m_keysFileLastModif )
        saveCurrentData();

    QFile m_tempFile(m_userFileName + "_tempDataFile");
    if ( m_tempFile.exists() ) {
        mergeFiles(m_userFileName, m_tempFile.fileName());
        if ( !m_tempFile.remove() )
            qDebug() << "Can't remove m_tempFile";
    }
}


template<class keyType, class valueType>
void BasicForge<keyType, valueType>::put(const keyType& userKey, const valueType& userValue)
{
    m_buffer.insert(userKey, userValue);

    if ( m_buffer.size() >= BUFFER_SIZE )
        saveCurrentData();
}


/*
 * This function saves all current user data in the file specified in the constructor
 */
template<class keyType, class valueType>
void BasicForge<keyType, valueType>::saveCurrentData()
{
    if ( m_buffer.isEmpty() ) {
        QFileInfo info(m_userFileName + "_keys");
        if ( info.lastModified() != m_keysFileLastModif )
            rewrite(QSet<keyType>());
        else
            return;
    }

    QFile m_file(m_userFileName);
    QFile m_keysFile(m_userFileName + "_keys");

    if ( !m_file.open(QIODevice::ReadWrite) )
        throw std::string("Error. Failed to open the file. saveCurrentData called mark#1");

    if( m_file.size() != 0 )
    {
        m_file.close();

        if ( !m_keysFile.open(QIODevice::ReadOnly) )
            throw std::string("Error. Failed to open the file. saveCurrentData called mark#2");

        QDataStream streamKeys(&m_keysFile);

        QSet<keyType> reKeys = m_buffer.keys().toSet();
        QSet<keyType> existingKeys;
        streamKeys >> existingKeys;
        reKeys &= existingKeys;

        existingKeys.clear(); //временно!
        m_keysFile.close();
        if ( streamKeys.status() != QDataStream::Ok )
            throw std::string("An error occurred while writing the Keys File. saveCurrentFile called mark#2");

        if ( !reKeys.isEmpty() )
            rewrite(reKeys);
        else {
            if ( !m_file.open(QIODevice::Append) )
                throw std::string("Error. Failed to open the file. saveCurrentData called mark#2");

            QDataStream streamFile(&m_file);
            streamFile << m_buffer;

            m_file.close();
            if ( streamFile.status() != QDataStream::Ok  )
                throw std::string("An error occurred while writing the file. saveCurrentData called");

            rewriteKeys(m_userFileName);
            QFileInfo info(m_keysFile);
            m_keysFileLastModif = info.lastModified();
        }

    }
    else // m_file is empty
    {
        if ( !m_keysFile.open(QIODevice::WriteOnly) )
            throw std::string("Error. Failed to open the keysFile. saveCurrentData called mark#3");

        QDataStream stream(&m_file);
        QDataStream streamKeys(&m_keysFile);

        stream << m_buffer;
        streamKeys << m_buffer.keys().toSet();

        m_file.close();
        m_keysFile.close();
        if ( stream.status() != QDataStream::Ok  )
            throw std::string("An error occurred while writing the file. saveCurrentData called");
        if ( streamKeys.status() != QDataStream::Ok  )
            throw std::string("An error occurred while writing the file. saveCurrentData called");


        QFileInfo info(m_keysFile);
        m_keysFileLastModif = info.lastModified();
    }

    m_buffer.clear();
}


template<class keyType, class valueType>
void BasicForge<keyType, valueType>::rewrite(const QSet<keyType>& reKeys)
{
    QFile file(m_userFileName);

    if ( !file.open(QIODevice::ReadWrite) )
        throw std::string("Error. Failed to open the file. rewrite called");

    if ( file.size() < SIZE_1GB )
    {
        file.close();
        rewriteData(m_userFileName, reKeys);
        rewriteKeys(m_userFileName);

        QFileInfo info(m_userFileName + "_keys");
        m_keysFileLastModif = info.lastModified();
    }
    else
    {
        QFile tempDataFile(m_userFileName + "_tempDataFile");

        if ( tempDataFile.exists() ) {
            if ( !tempDataFile.open(QIODevice::ReadWrite) )
                throw std::string("Error. Failed to open the file. saveCurrentData called mark#3");

            if ( tempDataFile.size() >= SIZE_1GB ) {
                file.close();
                mergeFiles(m_userFileName, tempDataFile.fileName());
                tempDataFile.close();
                if ( !tempDataFile.remove() )
                    throw std::string("Error. Failed to remove a tempDataFile. saveCurrentData called mark#3");
                if ( !tempDataFile.open(QIODevice::WriteOnly) )
                    throw std::string("Error. Failed to open a tempDataFile. saveCurrentData called mark#3");
                QDataStream streamTempDataFile(&tempDataFile);
                streamTempDataFile << m_buffer;
                if ( streamTempDataFile.status() != QDataStream::Ok )
                    throw std::string("An error occurred while writing the file. saveCurrentData called");

                rewriteKeys(m_userFileName);

                QFileInfo info(m_userFileName + "_keys");
                m_keysFileLastModif = info.lastModified();
            }
            else {
                rewriteData(tempDataFile.fileName(), reKeys);
                rewriteKeys(m_userFileName);

                QFileInfo info(m_userFileName + "_keys");
                m_keysFileLastModif = info.lastModified();
            }
        }
        else {
            if ( !tempDataFile.open(QIODevice::WriteOnly) )
                throw std::string("Error. Failed to open the tempDataFile. saveCurrentData called mark#3");
            QDataStream streamTempDataFile(&tempDataFile);
            streamTempDataFile << m_buffer;
            if ( streamTempDataFile.status() != QDataStream::Ok )
                throw std::string("An error occurred while writing the file. saveCurrentData called");

            rewriteKeys(m_userFileName);

            QFileInfo info(m_userFileName + "_keys");
            m_keysFileLastModif = info.lastModified();
        }
    }
}


/*
 * Overwrite previously existing values ​​with the same keys
 */
template<class keyType, class valueType>
void BasicForge<keyType, valueType>::rewriteData(const QString& fileName, const QSet<keyType>& keys)
{
    QFile keysFile(fileName + "_keys");

    if ( !keysFile.open(QIODevice::ReadOnly) )
        throw std::string("Error. Failed to open a keysFile. rewriteData called");
    QDataStream streamKeysFile(&keysFile);

    QSet<keyType> existingKeys;
    streamKeysFile >> existingKeys;

    keysFile.close();
    if ( streamKeysFile.status() != QDataStream::Ok )
        throw std::string("An error occurred while reading a keysFile. rewriteData called");


    QFile file(fileName);
    QFile tempFile(fileName + "_temp");

    if ( !file.open(QIODevice::ReadWrite) )
        throw std::string("Error. Failed to open a file. rewriteData called");
    if ( !tempFile.open(QIODevice::ReadWrite) )
        throw std::string("Error. Failed to open a temp file. rewriteData called");
    QDataStream stream(&file);
    QDataStream streamTempFile(&tempFile);

    qint64 startRewrite = -1;
    QHash<keyType, valueType> tempBuff;
    while ( !stream.atEnd() ) {
        qint64 prevPos = file.pos();
        stream >> tempBuff;
        QSet<keyType> temp = tempBuff.keys().toSet();
        temp -= existingKeys;
        if ( !temp.isEmpty() ) {
            if ( startRewrite == -1 )
                startRewrite = prevPos;
            foreach ( const keyType& i, temp )
                tempBuff.remove(i);
        }

        temp = tempBuff.keys().toSet();
        temp &= keys;
        if ( !temp.isEmpty() ) {
            if ( startRewrite == -1 )
                startRewrite = prevPos;
            foreach ( const keyType& i, temp ) {
                tempBuff[i] = m_buffer[i];
                m_buffer.remove(i);
            }
        }
        if ( startRewrite != -1 )
            streamTempFile << tempBuff;
    }

    tempBuff.clear();
    if ( !m_buffer.isEmpty() ) {
        foreach ( const keyType& i, m_buffer.keys() )
            tempBuff.insert(i, m_buffer.value(i));

        streamTempFile << tempBuff;
        tempBuff.clear();
    }

    if ( stream.status() != QDataStream::Ok )
        throw std::string("An error occurred while reading a file. rewriteData called");
    if ( streamTempFile.status() != QDataStream::Ok )
        throw std::string("An error occurred while writing a tempFile. rewriteData called");

    file.seek(startRewrite);
    tempFile.seek(0);
    while ( !streamTempFile.atEnd() ) {
        qint64 buffSize = tempFile.pos() + SIZE_512MB;
        if ( buffSize <= tempFile.size() )
            buffSize = SIZE_512MB;
        else
            buffSize = tempFile.size() - tempFile.pos();

        char* buff = new char[buffSize];
        tempFile.read(buff, buffSize);
        file.write(buff, buffSize);
        delete[] buff;
    }

    if ( file.pos() < file.size() )
        if ( !file.resize(file.pos()) )
            throw std::string("An error occurred while resizing a file. rewriteData called");

    file.close();
    tempFile.close();
    if ( stream.status() != QDataStream::Ok )
        throw std::string("An error occurred while writing the file. rewriteData called");
    if ( streamTempFile.status() != QDataStream::Ok )
        throw std::string("An error occurred while reading a tempFile. rewriteData called");


    if ( !tempFile.remove() )
        throw std::string("An error occurred while deleting the tempFile. rewriteData called");
}


template<class keyType, class valueType>
void BasicForge<keyType, valueType>::mergeFiles(const QString& toName, const QString& fromName)
{
    QFile firstFile(toName);
    QFile secondFile(fromName);
    QFile tempFile(toName + "_test");

    if ( !firstFile.open(QIODevice::ReadOnly) )
        throw std::string("Error. Failed to open a firstFile. mergeFiles called");
    if ( !secondFile.open(QIODevice::ReadOnly) )
        throw std::string("Error. Failed to open a secondFile. mergeFiles called");
    if ( !tempFile.open(QIODevice::WriteOnly) )
        throw std::string("Error. Failed to open a tempFile. mergeFiles called");

    QDataStream streamFirst(&firstFile);
    QDataStream streamSecond(&secondFile);
    QDataStream streamTempFile(&tempFile);

    QSet<keyType> reKeys;
    while ( !streamFirst.atEnd() ) {
        QHash<keyType, valueType> temp;
        streamFirst >> temp;
        reKeys += temp.keys().toSet();
    }
    firstFile.seek(0);
    while ( !streamSecond.atEnd() ) {
        QHash<keyType, valueType> temp;
        streamSecond >> temp;
        reKeys &= temp.keys().toSet();
    }
    secondFile.seek(0);

    QHash<keyType, valueType> tempBuff;
    QHash<keyType, valueType> secondTempBuff;
    while ( !streamSecond.atEnd() ) {
        streamSecond >> secondTempBuff;
        QSet<keyType> temp = secondTempBuff.keys().toSet();
        temp &= reKeys;
        if ( !temp.isEmpty() ) {
            foreach ( const keyType& i, temp ) {
                tempBuff.insert(i, secondTempBuff.value(i));
                secondTempBuff.remove(i);
            }
        }
        if ( tempBuff.size() >= BUFFER_SIZE ) {
            streamTempFile << tempBuff;
            tempBuff.clear();
        }
        if ( !secondTempBuff.isEmpty() )
            streamTempFile << secondTempBuff;
    }

    if ( !tempBuff.isEmpty() ) {
        streamTempFile << tempBuff;
        tempBuff.clear();
    }

    while ( !streamFirst.atEnd() ) {
        streamFirst >> tempBuff;
        QSet<keyType> temp = tempBuff.keys().toSet();
        temp &= reKeys;
        if ( !temp.isEmpty() ) {
            foreach ( const keyType& i, temp )
                tempBuff.remove(i);
        }

        if ( !tempBuff.isEmpty() )
            streamTempFile << tempBuff;
    }

    firstFile.close();
    secondFile.close();
    tempFile.close();

    if ( streamFirst.status() != QDataStream::Ok )
        throw std::string("An error occurred while reading a first file. mergeFiles called");
    if ( streamSecond.status() != QDataStream::Ok )
        throw std::string("An error occurred while reading a second file. mergeFiles called");
    if ( streamTempFile.status() != QDataStream::Ok )
        throw std::string("An error occurred while reading a tempFile. mergeFiles called");

    if ( !firstFile.remove() )
        throw std::string("An error occurred while removing a first file. mergeFiles called");

    if ( !tempFile.rename(toName) )
        throw std::string("An error occurred while renaming a temp file. mergeFiles called");

    if ( !isNormalizedFile(toName) )
        normalizeFile(toName);
}


template<class keyType, class valueType>
void BasicForge<keyType, valueType>::normalizeFile(const QString& fileName)
{
    QFile file(fileName);
    QFile tempFile(fileName + "_temp");
    QFile anotherTempFile(fileName + "_anotherTemp");

    if ( !file.open(QIODevice::ReadWrite) )
        throw std::string("Error. Failed to open a file. normalizeFile called");
    if ( !tempFile.open(QIODevice::WriteOnly) )
        throw std::string("Error. Failed to open a tempFile. normalizeFile called");
    if ( !anotherTempFile.open(QIODevice::WriteOnly) )
        throw std::string("Error. Failed to open a anotherTempFile. normalizeFile called");

    QDataStream stream(&file);
    QDataStream streamTempFile(&tempFile);
    QDataStream streamAnotherTempFile(&anotherTempFile);

    qint64 startPos = -1;
    QHash<keyType, valueType> firstBuff;
    while ( !stream.atEnd() ) {
        qint64 prevPos = file.pos();
        stream >> firstBuff;
        if ( firstBuff.size() < BUFFER_SIZE ) {
            if ( startPos == -1 )
                startPos = prevPos;

            streamTempFile << firstBuff;
        }
        else if ( startPos != -1 )
            streamAnotherTempFile << firstBuff;
    }

    firstBuff.clear();

    tempFile.close();
    anotherTempFile.close();

    if ( stream.status() != QDataStream::Ok )
        throw std::string("An error occurred while reading a file. normalizeFile called");
    if ( streamTempFile.status() != QDataStream::Ok )
        throw std::string("An error occurred while writing a tempFile. normalizeFile called");
    if ( streamAnotherTempFile.status() != QDataStream::Ok )
        throw std::string("An error occurred while writing a tempFile. normalizeFile called");

    if ( !anotherTempFile.open(QIODevice::ReadOnly) )
        throw std::string("Error. Failed to open a anotherTempFile. normalizeFile called");

    stream.device()->seek(startPos);
    streamAnotherTempFile.device()->reset();
    while ( !anotherTempFile.atEnd() ) {
        qint64 buffSize = 0;
        if ( anotherTempFile.size() - anotherTempFile.pos() >= SIZE_512MB )
            buffSize = SIZE_512MB;
        else
            buffSize = anotherTempFile.size() - anotherTempFile.pos();

        char* buff = new char[buffSize];
        anotherTempFile.read(buff, buffSize);
        file.write(buff, buffSize);
        delete[] buff;
    }

    anotherTempFile.close();
    if ( stream.status() != QDataStream::Ok )
        throw std::string("An error occurred while writing the file. normalizeFile called");
    if ( streamTempFile.status() != QDataStream::Ok )
        throw std::string("An error occurred while reading a tempFile. normalizeFile called");

    if ( !anotherTempFile.remove() )
        throw std::string("An error occurred while removing a tempFile. normalizeFile called");


    if ( !tempFile.open(QIODevice::ReadOnly) )
        throw std::string("Error. Failed to open a tempFile. normalizeFile called");
    streamTempFile.device()->reset();

    QHash<keyType, valueType> secondBuff;
    while ( !streamTempFile.atEnd() ) {
        streamTempFile >> firstBuff;
        foreach ( const keyType& i , firstBuff.keys() ) {
            secondBuff.insert(i, firstBuff.value(i));
            if ( secondBuff.size() >= BUFFER_SIZE ) {
                stream << secondBuff;
                secondBuff.clear();
            }
        }
    }

    if ( !secondBuff.isEmpty() )
        stream << secondBuff;

    if ( file.pos() < file.size() )
        if ( !file.resize(file.pos()) )
            throw std::string("An error occurred while resizing a file. normalizeFile called");

    file.close();
    tempFile.close();
    if ( stream.status() != QDataStream::Ok )
        throw std::string("An error occurred while writing the file. normalizeFile called");
    if ( streamTempFile.status() != QDataStream::Ok )
        throw std::string("An error occurred while reading a tempFile. normalizeFile called");

    if ( !tempFile.remove() )
        throw std::string("An error occurred while removing a tempFile. normalizeFile called");
}


template<class keyType, class valueType>
bool BasicForge<keyType, valueType>::isNormalizedFile(const QString& fileName) const
{
    QFile file(fileName);

    if ( !file.open(QIODevice::ReadOnly) )
        throw std::string("Error. Failed to open a file. isNormalizedFile called");

    QDataStream stream(&file);

    QHash<keyType, valueType> temp;
    int count = 0;
    while ( !stream.atEnd() ) {
        stream >> temp;
        if ( temp.size() < BUFFER_SIZE - 2000 )
            count++;
        if ( count >= 100 )
            return false;
    }

    file.close();
    if ( stream.status() != QDataStream::Ok )
        throw std::string("An error occurred while reading the file. normalizeFile called");

    return true;
}


template<class keyType, class valueType>
void BasicForge<keyType, valueType>::rewriteKeys(const QString& fileName)
{
    QFile keysFile(fileName + "_keys");

    if ( !keysFile.open(QIODevice::ReadWrite) )
        throw std::string("Error. Failed to open the keys file. rewriteKeys called");

    QDataStream streamKeys(&keysFile);

    QSet<keyType> existingKeys;
    streamKeys >> existingKeys;
    existingKeys += m_buffer.keys().toSet();
    keysFile.seek(0);
    streamKeys << existingKeys;

    if ( keysFile.pos() < keysFile.size() )
        if ( !keysFile.resize(keysFile.pos()) )
            throw std::string("An error occurred while resizing a keysFile. rewriteKeys called");

    keysFile.close();
    if ( streamKeys.status() != QDataStream::Ok )
        throw std::string("An error occurred while reading and writing the keys file. "
                          "rewriteKeys called");
}


template<class keyType, class valueType>
void BasicForge<keyType, valueType>::remove(const keyType& userKey)
{
    if ( m_buffer.contains(userKey) ) {
        m_buffer.remove(userKey);
        return;
    }

    else {
        QFile fileKeys(m_userFileName + "_keys");

        if ( !fileKeys.open(QIODevice::ReadWrite) )
            throw std::string("Error. Failed to open the key-file. remove called");
        QDataStream stream(&fileKeys);

        QSet<keyType> keys;
        stream >> keys;
        if ( keys.contains(userKey) ) {
            keys.remove(userKey);

            fileKeys.seek(0);
            stream << keys;

            if ( fileKeys.pos() < fileKeys.size() )
                if ( !fileKeys.resize(fileKeys.pos()) )
                    throw std::string("An error occurred while resizing a keysFile. remove called");
        }

        fileKeys.close();
        if ( stream.status() != QDataStream::Ok )
            throw std::string("An error occurred while reading the key-file. remove called");
    }
}


template<class keyType, class valueType>
bool BasicForge<keyType, valueType>::containsKey(const QString& fileName, const keyType& userKey) const
{
    QFile fileKeys(fileName + "_keys");

    if ( !fileKeys.exists() )
        return false;

    if ( !fileKeys.open(QIODevice::ReadOnly) ) {
        qDebug() << "Error. Failed to open the key-file. containsKey called";
        return false;
    }

    QDataStream stream(&fileKeys);
    QSet<keyType> temp;
    while ( !stream.atEnd() ) {
        stream >> temp;
        if ( temp.contains(userKey) )
            return true;
    }

    fileKeys.close();
    if ( stream.status() != QDataStream::Ok ) {
        qDebug() << "An error occurred while reading the key-file. containsKey called";
        return false;
    }

    return false;
}


template<class keyType, class valueType>
valueType BasicForge<keyType, valueType>::read(const keyType& userKey, const valueType& defaultValue) const
{
    static QHash<keyType, valueType> staticBuff;
    static QDateTime userFileLastModif;
    static QDateTime tempDataLastModif;

    if ( m_buffer.contains(userKey) )
        return m_buffer.value(userKey);

    else if ( staticBuff.contains(userKey) ) {
        QFileInfo info(m_userFileName);
        if ( info.lastModified() == userFileLastModif ) {
            info.setFile(m_userFileName + "_tempDataFile");
            if ( info.lastModified() == tempDataLastModif )
                return staticBuff.value(userKey);
            else
                tempDataLastModif = info.lastModified();
        }
        else
            userFileLastModif = info.lastModified();
    }


    if ( this->containsKey(m_userFileName, userKey) ) {
        QFile userFile(m_userFileName + "_tempDataFile");
        if ( !userFile.exists() )
            userFile.setFileName(m_userFileName);

        if ( !userFile.open(QIODevice::ReadOnly) ) {
            qDebug() << "Error. Failed to open the userFile. read called";
            return defaultValue;
        }

        QDataStream stream(&userFile);
        while ( !stream.atEnd() ) {
            stream >> staticBuff;
            if ( staticBuff.contains(userKey) )
                return staticBuff.value(userKey);
        }

        userFile.close();
        if ( stream.status() != QDataStream::Ok ) {
            qDebug() << "An error occurred while reading the file. read called";
            staticBuff.clear();
            return defaultValue;
        }

        stream.setDevice(0);
        stream.resetStatus();
        userFile.setFileName(m_userFileName);
        if ( !userFile.open(QIODevice::ReadOnly) ) {
            qDebug() << "Error. Failed to open the file. read called";
            return defaultValue;
        }
        stream.setDevice(&userFile);
        while ( !stream.atEnd() ) {
            stream >> staticBuff;
            if ( staticBuff.contains(userKey) )
                return staticBuff.value(userKey);
        }

        userFile.close();
        if ( stream.status() != QDataStream::Ok ) {
            qDebug() << "An error occurred while reading the file. read called";
            staticBuff.clear();
            return defaultValue;
        }
    }

    else
        return defaultValue;

    return defaultValue; //??
}


template<class keyType, class valueType>
void BasicForge<keyType, valueType>::setDefaultValue(const QString& defValue)
{
    QString temp = m_defaultValue;
    m_defaultValue = defValue;
    return temp;
}


template<class keyType, class valueType>
valueType BasicForge<keyType, valueType>::defaultValue() const
{
    return m_defaultValue;
}


template<class keyType, class valueType>
valueType BasicForge<keyType, valueType>::read(const keyType& userKey) const
{
    return read(userKey, m_defaultValue);
}


template<class keyType, class valueType>
bool BasicForge<keyType, valueType>::containsKey(const keyType& userKey) const
{
    if ( m_buffer.contains(userKey) )
        return true;
    else if ( containsKey(m_userFileName + "_keys") )
        return true;
    else
        return false;
}


template<class keyType, class valueType>
bool BasicForge<keyType, valueType>::isEmpty() const
{
    if ( !m_buffer.isEmpty() )
        return false;

    QFile keysFile(m_userFileName + "_keys");
    if ( !keysFile.exists() )
        return true;

    if ( !keysFile.open(QIODevice::ReadOnly) ) {
        qDebug() << "Failed to open a file";
        return false;
    }

    QDataStream stream(&keysFile);
    QSet<keyType> set;
    stream >> set;

    if ( set.isEmpty() )
        return true;
    else
        return false;
}

//void ForgeBase::saveInFile(const QString& fileName)
//{
//    QFile userFile( userFileName + ".dat" );
//    QFile userFileKeys( userFileName + ".key" );
//    QFile file( fileName + ".dat" );
//    QFile fileKeys( fileName + ".key" );
//    if ( userFile.exists() )
//    {
//        const QSet<int> tempSet = containsKeys( userFileName, keyCheck );
//        if ( !tempSet.empty() ) {
//            rewrite( userFileName, tempSet, Buffer );

//            for (QSet<int>::const_iterator i = tempSet.constBegin(); i != tempSet.constEnd(); ++i) {
//                keyCheck.remove(*i);
//                Buffer.remove(*i);
//            }
//        }

//        if ( file.exists() )
//            if ( !file.remove() )
//                throw std::string("Error. Failed to delete the file. saveInFile(name) called");

//        if ( fileKeys.exists() )
//            if ( !fileKeys.remove() )
//                throw std::string("Error. Failed to delete the file-keys. saveInFile(name) called");

//        if ( !userFile.copy( fileName + ".dat") )
//            throw std::string("Error. Failed to copy the temp file. saveInFile(name) called");

//        if ( !userFileKeys.copy( fileName + ".key") )
//            throw std::string("Error. Failed to copy the temp file-keys. saveInFile(name) called");
//    }

//    if ( !file.open(QIODevice::Append) )
//        throw std::string("Error. Failed to open the file. saveInFile(name) called");

//    if ( !fileKeys.open(QIODevice::Append) )
//        throw std::string("Error. Failed to open the file-keys. saveInFile(name) called");

//    QDataStream streamFile(&file);
//    QDataStream streamFileKeys(&fileKeys);

//    if ( !Buffer.isEmpty() )
//        streamFile << Buffer;
//    if ( !keyCheck.isEmpty() )
//        streamFileKeys << keyCheck;

//    file.close();
//    fileKeys.close();

//    if ( streamFile.status() != QDataStream::Ok  )
//        throw std::string("An error occurred while writing the temp file. saveInFile(name) called");
//    else if ( streamFileKeys.status() != QDataStream::Ok )
//        throw std::string("An error occurred while writing the temp temp file-keys. saveInFile(name) called");
//}


//void ForgeBase::setFile(const QString& fileName)
//{
//    if ( userFileName == "userData" )
//    {
//        QFile userFile( userFileName + ".dat" );
//        QFile userFileKeys( userFileName + ".key" );

//        if ( !userFile.rename( fileName + ".dat" ) )
//            throw std::string("Error. Failed to rename the userFile. setFile called");
//        if ( !userFileKeys.rename( fileName + ".key" ) )
//            throw std::string("Error. Failed to rename the userFile-keys. setFile called");

//        userFileName = fileName;
//    }

//    else
//    {
//        saveCurrentData();

//        QFile file( fileName + ".dat" );
//        if ( file.exists() )
//            userFileName = fileName;
//        else if ( file.open(QIODevice::ReadWrite) ) {
//            file.close();
//            file.remove();
//            userFileName = fileName;
//        }
//        else
//            throw std::string("Error. Failed to open the file. setFile called");
//    }
//}



//QVariant ForgeBase::readFromOtherFile(const QString& fileName, const int& user_key) const
//{
//    QFile file( fileName + ".dat" );
//    if ( !file.open(QIODevice::ReadOnly) )
//        throw std::string("Error. Failed to open the file. readFromOtherFile called");

//    if ( containsKey(fileName, user_key) )
//    {
//        QHash<int, QVariant> temp;
//        QDataStream stream(&file);
//        while ( !Buffer.contains(user_key) )
//            stream >> temp;

//        if (stream.status() != QDataStream::Ok)
//            throw std::string("An error occurred while reading the file. readFromOtherFile called called");

//        file.close();

//        return temp[user_key];
//    }
//    else
//        return QString("value not found");
//}


//QString ForgeBase::currentName() const
//{
//    return userFileName;
//}


//bool ForgeBase::existsFile(const QString& fileName) const
//{
//    QFile file( fileName + ".dat" );

//    if (userFileName == fileName)
//        return true;
//    else if ( file.exists() )
//        return true;
//    else
//        return false;
//}


//void ForgeBase::doScript()
//{
//    std::string str;
//    std::getline(std::cin, str);
//    ForgeUser forgeUser;
//    parse(str, this, &forgeUser);
//}




//void Forge::doScript(const bool& queryMode)
//{
//    if (CURRENT != this) //проверять не изменилось ли this
//        CURRENT = this;

//    if (queryMode)
//    {
//        qDebug() << "Attention! Then your work will be complete, enter \"exit\" to exit from query mode";
//                                              //поправить вывод этого сообщения, возможно добавить в начале Forge Help

//        std::string result;
//        while (result != "exit")
//        {
//            result.clear();
//            std::string line = "\\";
//            while ( *(line.end() - 1) == '\\' ) {
//                std::getline(std::cin, line);
//                result = result + line;
//                if ( *(line.end() - 1) == '\\' )
//                    result.erase( result.size() - 1, 1);
//            }

//            if (result != "exit")
//                if ( luaL_dostring(LState, result.c_str()) ) { /* If some error occured, then print error string */
//                    qDebug() << lua_tostring(LState, -1);
//                    lua_pop(LState, -1);
//                }
//        }
//    }

//    else /* if !queryMode, then enter all lua code in standart input */
//    {
//        char c = 'y';
//        while (c == 'y') { /* If some error occured, then print error string */

//            qDebug() << "Enter your lua script(Ctrl+D to complete the input):";
//            if ( luaL_dofile(LState, NULL) ) {
//                qDebug() << lua_tostring(LState, -1);
//                lua_pop(LState, 1);
//                qDebug() << "Enter 'y' if you want to start again, or enter any other key to finish";
//            }
//            else
//                qDebug() << "Enter 'y' if you want to start again, or enter any other key to finish";
//            std::cin >> c;
//        }
//    }

//    if ( cleanGlobalTable() )
//        qDebug() << "some error occured while cleaning the global table values";
//}


//void Forge::doScript(const QString& fileName)
//{
//    if (CURRENT != this) //проверять не изменилось ли this
//        CURRENT = this;

//    if ( luaL_dofile(LState, fileName.toStdString().c_str()) ) {
//        qDebug() << lua_tostring(LState, -1);
//        lua_pop(LState, -1);
//    }

//    if ( cleanGlobalTable() )
//        qDebug() << "some error occured while cleaning the global table values";
//}


//void Forge::doScriptString(const QString& line)
//{
//    if (CURRENT != this)
//        CURRENT = this;

//    if ( luaL_dostring(LState, line.toStdString().c_str()) ) {
//        qDebug() << lua_tostring(LState, -1);
//        lua_pop(LState, -1);
//    }

////    if ( cleanGlobalTable() )
////        qDebug() << "some error occured while cleaning the global table values";
//}


//QSet<QString> Forge::getLuaGlobalNames()
//{
//    std::string LuaCode =
//            "luaGlobalKeysString = \"\""
//            "for k, v in pairs(_G) do "
//            "luaGlobalKeysString = luaGlobalKeysString..k..\",\" "
//            "end " ;

//    luaL_dostring(LState, LuaCode.c_str() );
//    lua_getglobal(LState, "luaGlobalKeysString");

//    QString globalNamesStr = lua_tostring(LState, -1);
//    lua_pop( LState, lua_gettop(LState) ); /*comment here*/
//    globalNamesStr.remove(-1,1); /*comment here*/

//    QSet<QString> LuaGlobalNames;
//    for ( int i = 0; i < globalNamesStr.count(',') + 1; ++i )
//        LuaGlobalNames.insert( globalNamesStr.section(',', i, i) );
//    LuaGlobalNames.remove("luaGlobalKeysString");

//    return LuaGlobalNames;
//}


//bool Forge::cleanGlobalTable()
//{
//    QSet<QString> globalNames = getLuaGlobalNames();
//    globalNames -= LuaStandartGlobalNames;

//    if ( globalNames.isEmpty() )
//        return false;

//    QString LuaCode;
//    for ( QSet<QString>::const_iterator i = globalNames.constBegin();
//          i != globalNames.constEnd(); ++i )
//    {
//        QString temp = *i;
//        LuaCode = LuaCode + temp + "=nil ";
//    }

////    qDebug() << LuaCode;
//    return luaL_dostring( LState, LuaCode.toStdString().c_str() );
//}



///************************ Lua functions ************************/
//int Forge::Forge_connect(lua_State*)
//{
//    if ( lua_gettop(c_LState) != 1 ) {
//        qDebug() << "ERROR! Please enter just one argument";
//        return 0;
//    }

//    QString name = lua_tostring(c_LState, -1);
//    if ( CURRENT->existsFile(name) ) {
//        CURRENT->setFile( lua_tostring(c_LState ,-1) );
//    }
//    else
//        qDebug() << "ERROR! There is no such file";

//    return 0;
//}


//int Forge::Forge_get(lua_State*)
//{
//    QStringList requestedIndexes;
//    int returnValues = lua_gettop(c_LState);
//    for ( int i = 1; i < returnValues + 1; ++i )
//        requestedIndexes.push_back( lua_tostring(c_LState, i) );

//    lua_State* tempState = luaL_newstate(); /* if return values are more than one */
//    std::string name = CURRENT->currentName().toStdString() + ".lua";
//    luaL_dofile( c_LState, name.c_str() );

//    for (int i = 0; i < requestedIndexes.size(); ++i) {
//        int j = 0;
//        for( ; j < requestedIndexes[i].count('.') + 1; ++j ) /* number of section is equel to number of points plus one */
//        {
//            if (j == 0)
//                lua_getglobal( c_LState,
//                               requestedIndexes[i].section('.', 0, 0).toStdString().c_str() );
//            else
//                lua_getfield( c_LState, -1, requestedIndexes[i].section('.', j, j).toStdString().c_str() );

//            if ( lua_isnil(c_LState, -1) ) {
//                qDebug() << "there is no such field";
//                return 0;
//            }
//        }
//        lua_xmove(c_LState, tempState, 1); /* move value to temp state */
//        lua_pop(c_LState, j);
//    }

//    lua_xmove(tempState, c_LState, returnValues); /* move all values back to top of stack */
//    lua_close(tempState);

//    return returnValues;
//}


//int Forge::Forge_put(lua_State*)
//{
//    if ( lua_gettop(c_LState) != 2 ) {
//        qDebug() << "ERROR! Please enter two arguments: key and value";
//        return 0;
//    }

//    if ( !lua_isstring(c_LState, -1) ) {
//        qDebug() << "ERROR! Second argument is not number or string";
//        return 0;
//    }

//    if ( !lua_isnumber(c_LState, -2) ) {
//        qDebug() << "ERROR! First argument is not number";
//        return 0;
//    }

//    const double key = (double)lua_tonumber(c_LState, -2);
//    double difference = key - floor(key);
//    if ( difference != 0 )
//        qDebug() << "WARRING! Your entered key are not integer, so it real part will be deleted";

//    CURRENT->put( key, lua_tostring(c_LState, -1) );

//    return 0;
//}


//int Forge::Forge_read(lua_State*)
//{
//    const int argCount = lua_gettop(c_LState);

//    for ( int i = 1; i < argCount + 1; ++i ) {
//        if ( lua_isnumber(c_LState, i) ) {
//            qDebug() << "ERROR! Argument No " << i << " is not number. All arguments must be a integer numbers";
//            return 0;
//        }
//    }

//    QStack<int> keys;
//    for ( int i = 1; i < argCount + 1; ++i ) {
//        double key = (double)lua_tonumber(c_LState, i);
//        double difference = key - floor(key);
//        if ( difference != 0 )
//            qDebug() << "WARRING! Your entered key No" << i << " are not integer, so it real part will be deleted";

//        keys.push(key);
//    }

//    lua_pop(c_LState, argCount);

//    while( !keys.empty() ) {
//        std::string str = CURRENT->read( keys.pop() ).toStdString();
//        lua_pushstring( c_LState, str.c_str() );
//    }

//    return argCount;
//}

#endif // FORGE_CPP
