#ifndef FORGE_H
#define FORGE_H

#include <QString>
#include <QHash>
#include <QDateTime>

//extern "C" {
//#include <lua.h>
//#include <lauxlib.h>
//#include <lualib.h>
//}

//#define c_LState     CURRENT->LState

enum Sizes {
    SIZE_1GB = 200, //1073741824;
    SIZE_512MB = 536870912
};

template <class keyType = qulonglong, class valueType = QString>
class BasicForge
{

private:

    QHash<keyType, valueType>      m_buffer;
    static const int               BUFFER_SIZE = 5;
    QString                        m_userFileName;
    valueType                      m_defaultValue;

    QDateTime                      m_keysFileLastModif;

    void        rewrite(const QSet<keyType>& reKeys);
    void        rewriteData(const QString& fileName, const QSet<keyType>& keys);
    void        rewriteKeys(const QString& fileName);
    void        mergeFiles(const QString& toName, const QString& fromName);
    void        normalizeFile(const QString& fileName);
    bool        isNormalizedFile(const QString& fileName) const;
    bool        containsKey(const QString& fileName, const keyType& userKey) const;

public:

                BasicForge();
                BasicForge(const QString& fileName);
                ~BasicForge();
    void        put(const keyType& userKey, const valueType& userValue);
    void        saveCurrentData();
    valueType   read(const keyType& userKey, const valueType& defaultValue) const;
    valueType   read(const keyType& userKey) const;
    void        remove(const keyType& userKey);
    void        setDefaultValue(const QString& defValue);
    valueType   defaultValue() const;
    bool        containsKey(const keyType& userKey) const;
    bool        isEmpty() const;

//    void        saveInFile(const QString& fileName);
//    QVariant    readFromOtherFile(const QString& fileName, const int& user_key) const;
//    bool        removeKey(const int& user_key);
//    void        setFile(const QString& fileName);
//    bool        containsKey(const keyType& user_key) const;

//    QString     read(const int& user_key) const;
//    QString     currentName() const;
//    bool        existsFile(const QString& fileName) const;
//    void        doScript();

//    void    doScript(const bool& queryMode);
//    void    doScript(const QString& fileName);
//    void    doScriptString(const QString& line);
};

typedef BasicForge<> Forge;

#include <forge.cpp>

#endif // FORGE_H
