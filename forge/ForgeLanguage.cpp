#include <ForgeLanguage.h>
#include <QMultiMap>
#include <QStringList>
#include <QString>
#include <QDebug>
#include <iostream>
#include <qalgorithms.h>
#include <ForgeAuthorisation.h>

bool connect(const QString& param, Forge* forge, ForgeUser* forgeUser)
{
    if (param == "localhost")
    {
        qDebug() << "Forge > Please enter your login or enter \"new user\" to create a new one: ";
        std::string str;
        std::getline(std::cin, str);

        if (str == "new user")
            if ( createUser() )

        qDebug() << param;
        qDebug() << "from connect func";

        return true;
    }

    else {
        qDebug() << "not localhost";
        return true;
    }
}


bool showDatabase(const QString& param, Forge* forge, ForgeUser* forgeUser)
{
    if ( param.isEmpty() ) {
        qDebug() << "no name of DB";
        return false;
    }

    qDebug() << param;
    qDebug() << "from show database func";

    return true;
}


bool showDatabases(const QString& param, Forge* forge, ForgeUser* forgeUser)
{
    if ( !param.isEmpty() ) {
        qDebug() << "some error in showDatabase func";
        return false;
    }

    qDebug() << param;
    qDebug() << "from show databases func";

    return true;
}



bool isToken(const SimpleTokensType& tokens, const QString& str)
{

    if ( tokens.contains(str) )
        return true;

    return false;
}


bool paramStart(const QString& paramStr)
{
    if ( paramStr.startsWith('\"') )
        return true;

    if ( paramStr.startsWith('(') )
        return true;

    return false;
}


bool paramEnd(const QString& paramStr)
{
    if ( paramStr.endsWith('\"') )
        return true;

    if ( paramStr.endsWith(')') )
        return true;

    return false;
}


QString correctParam(const QString& query, const short& position, bool* error)
{
    short i = position;

    char ch;
    if ( query.section(' ', i, i).startsWith('\"') )
        ch = '\"';
    else if ( query.section(' ', i, i).startsWith('(') )
        ch = ')';

    QString result = query.section(' ', i, i);
    while ( !query.section(' ', i, i).endsWith(ch) )
    {
        //        qDebug() << query.section(' ', i, i);
        ++i;
        result += ' ';
        result += query.section(' ', i, i);
        if ( i == query.count(' ') + 1 && !query.section(' ',i, i).endsWith(ch) ) {
//            qDebug() << query.section(' ', i, i);
            *error = true;
            break;
        }
    }

//    if (*error == false) {
//        result.remove(0, 1);
//        result.remove( result.size() - 1, 1 );
//    }
//    result.remove( result.size() - 1, 1 );
    return result;
}


bool isEquivalent(const QString& one, const QString& two)
{
    if (one == two)
        return true;

    if ( one.startsWith('(') && two.startsWith('(') )
        return true;

    if ( one.startsWith('\"') && two.startsWith('\"') )
        return true;

    return false;
}


bool lessThan(const QStringList& one, const QStringList& two)
{
    return one.size() > two.size();
}

int getEndsNumberOfSingleCommand(const TokenRelationsBaseType& base, const QStringList& query)
{
    bool found = false;
    if ( !base.contains(query.first()) )
        return 0;

    QList<QStringList> values = base.values( query.first() );
    qSort( values.begin(), values.end(), lessThan );
    for (int i = 0; i < values.size(); ++i)
    {
        int j = 0;
        for ( ; j < values.at(i).count(); ++j) {
            if ( !isEquivalent(values.at(i).at(j), query.value(j)) )
                break;
            else if ( j == values.at(i).size() - 1 )
                found = true;
        }

        if (found)
            return j;
    }

    return 0;
}


TokenRelationsBaseType initRelationsBase()
{
    TokenRelationsBaseType base;

    QStringList list;
    list << "connect" << "\"\"";
    base.insert("connect", list);

    list.clear();
    list << "show" << "database" << "\"\"";
    base.insert("show", list);

    list.clear();
    list << "show" << "database";
    base.insertMulti("show", list);

    list.clear();
    list << "show" << "databases";
    base.insertMulti("show", list);

    return base;
}


TokensBaseType initTokensBase()
{
    TokensBaseType base;

    base.insert("connect", connect);
    base.insert("show database", showDatabase);
    base.insert("show databases", showDatabases);

    return base;
}


SimpleTokensType initSimpleTokens()
{
    SimpleTokensType simpleTokens;

    simpleTokens.insert("connect");
    simpleTokens.insert("show");
    simpleTokens.insert("database");
    simpleTokens.insert("databases");

    return simpleTokens;
}


bool parse(const std::string& queryStr, Forge* forge, ForgeUser* forgeUser)
{
    static const TokenRelationsBaseType relationsBase = initRelationsBase();
    static const TokensBaseType tokensBase = initTokensBase();
    static const SimpleTokensType simpleTokens = initSimpleTokens();

    QString query( queryStr.c_str() );
    QStringList sequence;
    bool start = false;
    for (short i = 0; i < query.count(' ') + 1; ++i)
    {
        if ( i == 0 && !isToken(simpleTokens, query.section(' ', 0, 0)) ) {
            qDebug() << "Error! Wrong query. Query must start with command";
            return false;
        }

        if ( isToken(simpleTokens, query.section(' ', i, i)) )
            sequence << query.section(' ', i, i);

        else if ( paramStart(query.section(' ', i, i)) ) {
            bool error = false;
            QString temp = correctParam(query, i, &error);
            if (error) {
                qDebug() << "Error! Wrong query. Parametr";
                return false;
            }
            else {
                start = true;
                sequence << temp;
            }
        }

        else if (start)
            continue;

        else if ( paramEnd(query.section(' ', i, i)) )
            start = false;

        else {
            qDebug() << "Error! Wrong query. Not command and not param";
            return false;
        }

    }

    qDebug() << sequence;

    QStringList copy = sequence;
    QStringList commands;
    QStringList params;
    while ( !copy.empty() )
    {
        int i = getEndsNumberOfSingleCommand(relationsBase, copy);
        qDebug() << i;
        if (i == 0) {
            qDebug() << "Error! Wrong query. Query is not correct";
            return false;
        }
        else {
            QString str;
            for (int j = 0; j < i; ++j) {
                if ( isToken(simpleTokens, copy.at(0)) ) {
                    str.push_back( copy.at(0) );
                    str.push_back(' ');
                }
                else {
                    str.remove( str.size() - 1, 1 );
                    commands << str;
                    str.clear();
                    params << copy.at(0);
                }
                copy.removeAt(0);
            }
            if ( !str.isEmpty() ) {
                str.remove( str.size() - 1, 1 );
                commands << str;
                params << "";
            }
        }
    }

    qDebug() << commands;
    qDebug() << params;
    //функция корректировки и выполнение

    if ( commands.size() != params.size() ) {
        qDebug() << "some error occured";
        return false;
    }

    for (int i = 0; i < commands.size(); ++i) {
        if ( !tokensBase.value(commands.at(i))(params.at(i), forge, forgeUser) )
            return false;
    }

    return true;
}
