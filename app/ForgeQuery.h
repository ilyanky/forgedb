#ifndef FORGEQUERY_H
#define FORGEQUERY_H

#include <BasicForge.h>
#include <ForgeUser.h>

typedef bool(*ForgeFoo)(const QString&, Forge*, ForgeUser*);

typedef QSet<QString>               SimpleTokensType;
typedef QMap<QString, ForgeFoo>     TokensBaseType;
typedef QMap<QString, QStringList>  TokenRelationsBaseType;

bool connect(const QString& param, Forge* forge, ForgeUser* forgeUser);
bool showDatabase(const QString& param, Forge* forge, ForgeUser* forgeUser);
bool showDatabases(const QString& param, Forge* forge, ForgeUser* forgeUser);


bool                    isToken(const SimpleTokensType& tokens, const QString& str);
bool                    paramStart(const QString& paramStr);
bool                    paramEnd(const QString& paramStr);
QString                 correctParam(const QString& query, const short& position, bool* error = 0);
bool                    isEquivalent(const QString& one, const QString& two);
bool                    lessThan(const QStringList& one, const QStringList& two);
int                     getEndsNumberOfSingleCommand(const TokenRelationsBaseType& base, const QStringList& query);
TokenRelationsBaseType  initRelationsBase();
TokensBaseType          initTokensBase();
SimpleTokensType        initSimpleTokens();
bool                    parse(const std::string& queryStr, Forge* forge, ForgeUser* forgeUser);

#endif // FORGEQUERY_H
