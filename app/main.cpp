#include <BasicForge.h>
#include <iostream>
#include <QDebug>
//#include <ForgeUser.h>
#include <QFile>
#include <QDataStream>
#include <QHash>
#include <QFileInfo>
#include <QList>
#include <QString>
#include <QDateTime>
#include <ForgeAuthorisation.h>

// id не должен быть равен 0

int main()
{
//    Forge forge("some");
    try {
        UserInfo info;
        info.id = 14;
        info.path = "somePath";
        info.forgeCount = 2;
        info.password = "somePswd";

        ForgeUser user("ilyanky");
        qDebug() << user.addInfo(info);

        /*
        user = {
            id = nil,
            login = nil,
            password = nil,
            path = nil,
            forgeCount = nil,
            forges = { }
           }
        */

//        QString str = "someUser = {"
//                "id = 1,"
//                "login = \"someUser\","
//                "password = \"\","
//                "path = \"\","
//                "forgeCount = 2,"
//                "forges = { {}, {} } }";

//        qDebug() << ForgeUser::isValidTable(str, "someUser");


//        createUser();

        qDebug() << user.getLuaTable();


    } catch(std::string& str) { qDebug() << str.c_str(); }


    return 0;
}



//        forge.readAll();

//        forge.put(1, "this is key numer 1");          //---
//        forge.put(2, "this is key numer 2");              //  ---
//        forge.put(3, "this is key numer 3");           //  ---
//        forge.put(4, "this is key numer 4");            //  --
//        forge.put(5, "this is key numer 5");               //   ---

//        forge.put(4, "this is test");                      //    ---
//        forge.put(5, "this is test of 5");                  //   ---

//        forge.put(6, "this is key numer 6");                //   ----
//        forge.put(7, "this is key numer 7");                //   ---
//        forge.put(8, "this is key numer 8");               //   ---
//        forge.put(9, "this is key numer 9");                //    ---
//        forge.put(10, "this is key numer 10");              //    ---

//        forge.put(7, "this is parta");                      //   ---
//        forge.put(8, "this is sparta");                     //   ---
//        forge.put(9, "this is sup");                        //   ---

//        forge.put(11, "this is key numer 11");              //  ---
//        forge.put(12, "this is key numer 12");              //  --
//        forge.put(13, "this is key numer 13");          //   --
//        forge.put(14, "this is key numer 14");          //   --
//        forge.put(15, "this is key numer 15");          //   --

//        forge.put(8, "this is crash");                 //  ----

//        forge.put(8, "maybe 8");
//        forge.remove(1);

//        forge.readAll();

//        qDebug() << forge.read(9, "there is no dark side of the moon really");
//        forge.readAll();


//        forge.put(1, "this is key numer 1 test");          //---
//        forge.put(2, "this is key numer 2 test");              //  ---
//        forge.put(3, "this is key numer 3 test");           //  ---
//        qDebug() << forge.read(5, "there is no dark side of the moon really");
//        qDebug() << forge.read(3, "there is no dark side of the moon really");
//        forge.put(4, "this is key numer 4 test");            //  --
//        forge.put(5, "this is key numer 5 test");               //   ---
//        qDebug() << forge.read(10, "there is no dark side of the moon really");

//        qDebug() << forge.read(4, "there is no dark side of the moon really");
//        forge.put(4, "this is test test");                      //    ---
//        forge.put(5, "this is test of 5");                  //   ---
//        qDebug() << forge.read(4, "there is no dark side of the moon really");

//        forge.put(6, "this is key numer 6");                //   ----
//        forge.put(7, "this is key numer 7");                //   ---
//        forge.put(8, "this is key numer 8");               //   ---
//        forge.put(9, "this is key numer 9");                //    ---
//        forge.put(10, "this is key numer 10");              //    ---

//        qDebug() << forge.read(4, "there is no dark side of the moon really");
//        qDebug() << forge.read(9, "there is no dark side of the moon really");
//        forge.put(7, "this is parta");                      //   ---
//        forge.put(8, "this is sparta");                     //   ---
//        forge.put(9, "this is sup test");                        //   ---
//        qDebug() << forge.read(9, "there is no dark side of the moon really");

//        forge.put(11, "this is key numer 11");              //  ---
//        forge.put(12, "this is key numer 12");              //  --
//        forge.put(13, "this is key numer 13");          //   --
//        forge.put(14, "this is key numer 14");          //   --
//        forge.put(15, "this is key numer 15");          //   --

//        qDebug() << forge.read(8, "there is no dark side of the moon really");
//        forge.put(8, "this is crash tst");                 //  ----
//        qDebug() << forge.read(8, "there is no dark side of the moon really");
