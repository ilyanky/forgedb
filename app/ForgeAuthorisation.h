#ifndef FORGEAUTHORISATION_H
#define FORGEAUTHORISATION_H

#include <ForgeUser.h>
#include <iostream>

ForgeUser initUser(const std::string& login);
bool containsUser(const std::string& login);
bool isCorrectPassword(const std::string& password, const std::string& login);
bool createUser();

#endif // FORGEAUTHORISATION_H
