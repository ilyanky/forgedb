#ifndef FORGEUSER_H
#define FORGEUSER_H

extern "C" {
#include <lauxlib.h>
#include <lua.h>
#include <lualib.h>
}
#include <QString>

//this is test

enum UserError {
    NoError,
    NoStandartTable,
    NoTable,
    LoginAlreadyExists,
    PasswordAlreadyExists,
    IdAlreadyExists,
    PathAlreadyExists,
    ForgeCountAlreadyExists
};


struct UserInfo
{
    ulong id;
    ulong forgeCount;
    std::string login;
    std::string password;
    std::string path;

    UserInfo()
    {
        id = 0;
        forgeCount = -1;
    }
};


class ForgeUser
{
private:
    std::string m_tableName;
    lua_State* mp_luaState;

    inline void throwExceptin(const std::string& excpStr);
    std::string initEmptyUser(const std::string& loginAndTableName) const;

public:
    ForgeUser(const std::string& usrTableName = "user");
    ForgeUser(const std::string& login, const std::string& password);
    ~ForgeUser();
    UserError addInfo(const UserInfo& info);
    QString getLuaTable() const;
    UserError addLogin(const std::string& login);
    UserError addPassword(const std::string& password);

    static bool isValidTable(const QString& luaTable, const std::string& tableName);
};

#endif // FORGEUSER_H
