#include "ForgeUser.h"
#include <QDebug>

ForgeUser::ForgeUser(const std::string& usrTableName) //= "user" by default
{
    mp_luaState = luaL_newstate();
    if (!mp_luaState)
        throwExceptin("some allocate error");

    const std::string luaTable = initEmptyUser(usrTableName);

    if ( luaL_dostring(mp_luaState, luaTable.c_str()) )
        throwExceptin("some errors while construct an empty ForgeUser");

    m_tableName = usrTableName;
}


ForgeUser::ForgeUser(const std::string& login, const std::string& password)
{
    mp_luaState = luaL_newstate();
    if (!mp_luaState)
        throwExceptin("some allocate error");

    const std::string luaTable = initEmptyUser(login);

    if ( luaL_dostring(mp_luaState, luaTable.c_str()) )
        throwExceptin("some errors while construct an empty ForgeUser");

    m_tableName = login;

    addPassword(password);
}


ForgeUser::~ForgeUser()
{
    if ( mp_luaState )
        lua_close(mp_luaState);
}


/*inline*/ void ForgeUser::throwExceptin(const std::string& excpStr) //перенести в хедер
{
    lua_close(mp_luaState);
    throw std::string(excpStr);
}


std::string ForgeUser::initEmptyUser(const std::string& loginAndTableName) const
{
    /*Template for any user of ForgeDB*/
    std::string luaTable(loginAndTableName);
    luaTable += " = { \n\t"
                  "id = 0, \n\t"
                  "login = \"";
    luaTable += loginAndTableName;
    luaTable += "\", \n\t"
            "password = \"\", \n\t"
            "path = \"\", \n\t"
            "forgeCount = -1, \n\t"
            "forges = {} \n"
            "}";

    return luaTable;
}


UserError ForgeUser::addInfo(const UserInfo& info)
{
    lua_getglobal(mp_luaState, m_tableName.c_str());
    if ( lua_isnil(mp_luaState, -1) )
        return NoStandartTable;

    if ( !info.login.empty() )
    {
        if ( m_tableName != "user" )
            return LoginAlreadyExists;

        /*Copy table with name equal login.*/
        lua_setglobal(mp_luaState, info.login.c_str());
        lua_pushnil(mp_luaState);
        lua_setglobal(mp_luaState, "user"); /*user = nil*/

//        lua_getfield(p_luaState, -1, "login");
//        if ( !lua_isnil(p_luaState, -1) ) {
//            lua_remove(p_luaState, -1);
//            lua_remove(p_luaState, -1);
//            return LoginAlreadyExists;
//        }

//        lua_remove(p_luaState, -1);
        lua_getglobal(mp_luaState, info.login.c_str());
        lua_pushstring(mp_luaState, info.login.c_str());
        lua_setfield(mp_luaState, -2, "login");

        m_tableName = info.login;
    }

    if ( info.id != 0 )
    {
        lua_getfield(mp_luaState, -1, "id");
        if ( (qint64)lua_tonumber(mp_luaState, -1) != 0 ) {
            lua_remove(mp_luaState, -1);
            lua_remove(mp_luaState, -1);
            return IdAlreadyExists;
        }

        lua_remove(mp_luaState, -1);
        lua_pushnumber(mp_luaState, info.id);
        lua_setfield(mp_luaState, -2, "id");
    }

    if ( !info.password.empty() )
    {
        lua_getfield(mp_luaState, -1, "password");
        std::string str(lua_tostring(mp_luaState, -1));
        if ( !str.empty() ) {
            lua_remove(mp_luaState, -1);
            lua_remove(mp_luaState, -1);
            return PasswordAlreadyExists;
        }

        lua_remove(mp_luaState, -1);
        lua_pushstring(mp_luaState, info.password.c_str());
        lua_setfield(mp_luaState, -2, "password");
    }

    if ( !info.path.empty() )
    {

        lua_getfield(mp_luaState, -1, "path");
        std::string str(lua_tostring(mp_luaState, -1));
        if ( !str.empty() ) {
            lua_remove(mp_luaState, -1);
            lua_remove(mp_luaState, -1);
            return PathAlreadyExists;
        }

        lua_remove( mp_luaState, -1 );
        lua_pushstring( mp_luaState, info.path.c_str() );
        lua_setfield(mp_luaState, -2, "path");
    }

    if ( !info.forgeCount != -1 )
    {
        lua_getfield(mp_luaState, -1, "forgeCount");
        if ( (qint64)lua_tonumber(mp_luaState, -1) != -1 ) {
            lua_remove(mp_luaState, -1);
            lua_remove(mp_luaState, -1);
            return ForgeCountAlreadyExists;
        }

        lua_remove( mp_luaState, -1 );
        lua_pushnumber( mp_luaState, info.forgeCount );
        lua_setfield(mp_luaState, -2, "forgeCount");
    }

    lua_remove(mp_luaState, -1);
    return NoError;
}


UserError ForgeUser::addLogin(const std::string& login)
{
    UserInfo info;
    info.login = login;
    return addInfo(info);
}


UserError ForgeUser::addPassword(const std::string& password)
{
    UserInfo info;
    info.password = password;
    return addInfo(info);
}


QString ForgeUser::getLuaTable() const
{
    QString table(m_tableName.c_str());
    table.push_back(" = { \n\t"
                    "id = ");

    lua_getglobal(mp_luaState, m_tableName.c_str());
    lua_getfield(mp_luaState, -1, "id");
    qint64 number = lua_tonumber(mp_luaState, -1);
    table.push_back(std::to_string(number).c_str());
    table.push_back(", \n\t"
                    "login = ");
    lua_remove(mp_luaState, -1);


    lua_getfield(mp_luaState, -1, "login");
    table.push_back(lua_tostring(mp_luaState, -1));
    table.push_back(", \n\t"
                    "password = ");
    lua_remove(mp_luaState, -1);


    lua_getfield(mp_luaState, -1, "password");
    table.push_back(lua_tostring(mp_luaState, -1));
    table.push_back(", \n\t"
                    "path = ");
    lua_remove(mp_luaState, -1);


    lua_getfield(mp_luaState, -1, "path");
    table.push_back(lua_tostring(mp_luaState, -1));
    table.push_back(", \n\t"
                    "forgeCount = ");
    lua_remove(mp_luaState, -1);


    lua_getfield(mp_luaState, -1, "forgeCount");
    number = lua_tonumber(mp_luaState, -1);
    table.push_back(std::to_string(number).c_str());
    table.push_back(", \n\t"
                    "forges = { \n\t\t");
    lua_remove(mp_luaState, -1);


    lua_getfield(mp_luaState, -1, "forges");
    for ( qint64 i = 1; i <= number; ++i) {
        table.push_back("{}, \n\t\t");

        //normal realization later
    }
    table.remove(table.size() - 5, 5);
    table.push_back(" \n\t"
                    "} \n"
                    "}");
    lua_remove(mp_luaState, lua_gettop(mp_luaState));

    return table;
}


/*static*/ bool ForgeUser::isValidTable(const QString& luaTable, const std::string& tableName)
{
    lua_State* tempState = luaL_newstate();
    if (!tempState)
        throw std::string("some allocate error in isValidTable func");
    luaL_openlibs(tempState);

    if ( luaL_dostring(tempState, luaTable.toStdString().c_str()) ) {
        qDebug() << lua_tostring(tempState, -1); //Добавить немного форматирования: приписать что-нибудь
        lua_close(tempState);
        return false;
    }
    lua_getglobal(tempState, tableName.c_str());

    /*Id check*/
    lua_getfield(tempState, -1, "id");
    if ( lua_isnil(tempState, -1) || !lua_isnumber(tempState, -1) ) {
        lua_close(tempState);
        return false;
    }
    lua_remove(tempState, -1);

    /*Login check*/
    lua_getfield(tempState, -1, "login");
    if ( !lua_isnil(tempState, -1) ) {
        if ( !lua_isstring(tempState, -1) ) {
            lua_close(tempState);
            return false;
        }
        else {
            std::string name = lua_tostring(tempState, -1);
            if ( name != tableName ) {
                lua_close(tempState);
                return false;
            }
        }
    }
    else {
        lua_close(tempState);
        return false;
    }
    lua_remove(tempState, -1);

    /*Password check*/
    lua_getfield(tempState, -1, "password");
    if ( lua_isnil(tempState, -1) || !lua_isstring(tempState, -1) ) {
        lua_close(tempState);
        return false;
    }
    lua_remove(tempState, -1);

    /*Path check*/
    lua_getfield(tempState, -1, "path");
    if ( lua_isnil(tempState, -1) || !lua_isstring(tempState, -1) ) {
        lua_close(tempState);
        return false;
    }
    lua_remove(tempState, -1);

    /*ForgeCount check*/
    qint64 forgeCount;
    lua_getfield(tempState, -1, "forgeCount");
    if ( lua_isnil(tempState, -1) || !lua_isnumber(tempState, -1) ) {
            lua_close(tempState);
            return false;
    }
    else
        forgeCount = lua_tonumber(tempState, -1);
    lua_remove(tempState, -1);

    /*Forges check*/
    lua_getfield(tempState, -1, "forges");
    if ( !lua_isnil(tempState, -1) ) {
        if ( !lua_istable(tempState, -1) ) {
            lua_close(tempState);
            return false;
        }
        else {
            std::string code = "count = #";
            code += tableName;
            code += ".forges;";
            luaL_dostring(tempState, code.c_str());
            lua_getglobal(tempState, "count");
            qint64 count = (qint64)lua_tonumber(tempState, -1);
            if ( count != forgeCount ) {
                lua_close(tempState);
                return false;
            }
            lua_remove(tempState, -1);
            for ( qint64 i = 1; i <= forgeCount; ++i) {
                code = "a = ";
                code += tableName;
                code += ".forges[";
                code += std::to_string(i);
                code += "]";
                qDebug() << luaL_dostring(tempState, code.c_str());
                lua_getglobal(tempState, "a");
                if ( lua_isnil(tempState, -1) || !lua_istable(tempState, -1) ) {
                    lua_close(tempState);
                    return false;
                }
                lua_remove(tempState, -1);
            }
        }
    }
    else {
        lua_close(tempState);
        return false;
    }
    lua_remove(tempState, -1);


    lua_close(tempState);
    return true;
}


/*
user = {
    id = nil,
    login = nil,
    password = nil,
    path = nil,
    forgeCount = nil,
    forges = { }
   }
*/
